# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class FjggfwItem(scrapy.Item):
    table = 't_tender_notice'
    id = scrapy.Field()
    url = scrapy.Field()
    # 行业
    industry = scrapy.Field()
    # 地区
    area = scrapy.Field()
    # 项目名称
    project_name = scrapy.Field()
    # 公告类型
    type = scrapy.Field()
    # 招标时间
    tender_time = scrapy.Field()
    # 开标时间
    open_time = scrapy.Field()
    tm = scrapy.Field()
    # 交易中心
    trading_center = scrapy.Field()
    content_text = scrapy.Field()

